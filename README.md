# HAC - Home Automation Control

HAC is a backend API server designed for the smart management of home
automation using relay devices. It allows users to monitor and control multiple
relays through a simple API interface, facilitating the automation of various
home systems.

## Overview

The application is developed in Python, leveraging the FastAPI framework to
create a fast, asynchronous API service. The architecture is focused on
simplicity and efficiency, with a main.py file containing the API logic and a
requirements.txt file for dependency management. This ensures a straightforward
setup and high performance during operation.

## Features

- **Relay Management:** HAC supports API endpoints (`/get`, `/set`, `/version`)
  to manage relay states and monitor system status.
- **FastAPI Framework:** Utilizes FastAPI for its asynchronous request handling
  capabilities, offering quick response times for home automation tasks.
- **Ease of Use:** Designed to be easy to set up and interact with, simplifying
  the process of home automation management.

## Getting Started

### Requirements

- Python 3.6 or newer
- FastAPI
- Uvicorn
- All project dependencies listed in `requirements.txt`.

### Quickstart

1. Install Python 3.6 or newer.
2. Install the required packages with `pip install -r requirements.txt`.
3. Run the application using Uvicorn with the command: `uvicorn main:app
   --reload`

## API Documentation

The HAC application provides a simple yet powerful set of API endpoints for
managing and monitoring relay devices within a home automation system. Below is
an overview of the available endpoints:

- `/get`: Retrieve the current status of one or more relays.

  Example:
  ```bash
  curl -X 'GET' 'http://127.0.0.1:8000/get?id=*' -H 'accept: application/json'
  ```

- `/set`: Change the state of a specific relay.

  Example:
  ```bash
  curl -X 'POST' 'http://127.0.0.1:8000/set' \
  -H 'Content-Type: application/json' \
  -d '{"id": "relay1", "state": "on"}'
  ```

- `/version`: Returns the current version of the HAC application.

  Example:
  ```bash
  curl -X 'GET' 'http://127.0.0.1:8000/version' -H 'accept: application/json'
  ```

Remember to replace `127.0.0.1:8000` with your actual server address and port
where the API is being served.

### License

MIT License

Copyright (c) 2024 f2hex. All rights reserved.
