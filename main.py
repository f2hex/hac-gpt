from fastapi import FastAPI, HTTPException, Query, Body
from pydantic import BaseModel
import logging
import traceback

relay_states = {'relay1': 'off', 'relay2': 'on', 'relay3': 'off'}

app = FastAPI()

logging.basicConfig(level=logging.INFO)

@app.get("/")
def read_root():
    try:
        return {"Server": "HAC"}
    except Exception as e:
        logging.error(f'Error occurred: {e}', exc_info=True)
        raise HTTPException(status_code=400, detail='Failed to process request')

# Modified function to simulate fetching relay status using the global relay_states
async def get_relay_status(channel='*'):
    if channel == '*':
        logging.info(f"Retrieving status for all relays: {relay_states}") # gpt_pilot_debugging_log
        return relay_states
    if channel in relay_states:
        logging.info(f"Retrieving status for relay {channel}: {relay_states[channel]}") # gpt_pilot_debugging_log
        return {channel: relay_states[channel]}
    else:
        raise ValueError(f'Channel {channel} not found.')

@app.get('/get')
async def get_status(id: str = Query('*', alias='id', description='The relay ID to get status for, use `*` for all')):
    try:
        logging.info(f'Fetching relay status for ID: {id}') # gpt_pilot_debugging_log
        if id == '*':
            logging.info('Retrieving statuses for all relays') # gpt_pilot_debugging_log
            return relay_states
        elif id in relay_states:
            logging.info(f'Retrieving status for relay {id}: {relay_states[id]}') # gpt_pilot_debugging_log
            return {id: relay_states[id]}
        else:
            error_message = f'ID {id} not found.'
            logging.error(error_message) # gpt_pilot_debugging_log
            raise HTTPException(status_code=404, detail=error_message)
    except ValueError as ve:
        logging.error(f'ID not found: {ve}\n{traceback.format_exc()}') # gpt_pilot_debugging_log
        raise HTTPException(status_code=404, detail=str(ve))
    except Exception as e:
        logging.error(f'Error occurred while fetching relay status: {e}\n{traceback.format_exc()}') # gpt_pilot_debugging_log
        raise HTTPException(status_code=400, detail='Failed to retrieve relay status')

class RelaySetting(BaseModel):
    id: str
    state: str

# Modified function to update relay status with additional logging
async def set_relay_status(relay_id: str, state: str):
    logging.info(f"Current relay states before update: {relay_states}") # gpt_pilot_debugging_log
    if relay_id in relay_states:
        relay_states[relay_id] = state
        logging.info(f"Relay {relay_id} now has the state '{state}'. Updated relay_states: {relay_states}") # gpt_pilot_debugging_log
        return {'message': f'Relay {relay_id} set to {state}'}
    else:
        logging.error(f"Attempting to update non-existent relay ID: {relay_id}. Current relay states: {relay_states}") # gpt_pilot_debugging_log
        raise ValueError(f'Relay {relay_id} not found')

@app.post('/set')
async def set_status(setting: RelaySetting):
    try:
        if setting.state not in ['on', 'off']:
            raise ValueError("State must be 'on' or 'off'.")
        if setting.id not in relay_states:
            error_message = f"Relay {setting.id} not found."
            logging.error(error_message) # gpt_pilot_debugging_log
            raise HTTPException(status_code=404, detail=error_message)
        result = await set_relay_status(setting.id, setting.state)
        return result
    except ValueError as ve:
        logging.error(f'Invalid request: {ve}', exc_info=True) # gpt_pilot_debugging_log
        raise HTTPException(status_code=400, detail=str(ve))
    except Exception as e:
        logging.error(f'Error occurred while setting relay status: {e}', exc_info=True) # gpt_pilot_debugging_log
        raise HTTPException(status_code=400, detail='Failed to set relay status')

@app.get('/version')
def get_version():
    try:
        logging.info('Fetching app version') # gpt_pilot_debugging_log
        return {"version": "1.0.0"}
    except Exception as e:
        logging.error(f'Error occurred while fetching version: {e}', exc_info=True) # gpt_pilot_debugging_log
        raise HTTPException(status_code=400, detail='Failed to retrieve version')
